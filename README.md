# RPG Characters

A console application in C# with unit tests, created for an Assignment.

## Description

The application features a basic console user interface for creating a new Character based on RPG elements with the following features:

-   Give your character a name and a class.

-   View your characters stats.

-   Level up your character.

-   Equip Items like Armor and Weapons.

-   Equipped Items determine your stats dynamically.

## Installation and Usage

```
1. git clone git@gitlab.com:morten.bay/rpgcharacters.git
2. Open the project file in Visual Stuidio 2022 and build / run the project.
```

## Created by

-   [MBN @morten.bay](@morten.bay)

## License

Noroff Accelerate, 2022.
