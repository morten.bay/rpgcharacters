using RPGCharacters.Characters;
using RPGCharacters.Characters.Classes;
using RPGCharacters.Shared;

namespace RPGCharactersTest
{
    public class CharacterTests
    {

        /// <summary>
        /// 1)  A character is level 1 when created.
        /// </summary>
        [Fact]
        public void Constructor_ObjectCreated_LevelShouldReturn1AsInt()
        {
            // Arrange
            var character = new Mage("Gandalf"); // Mage class is used since we cannot instantiate from the abstract base class Character
            int expected = 1;

            // Act
            int actual = character.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 2) When a character gains a level, it should be level 2.
        /// </summary>
        [Fact]
        public void LevelUp_CalledOnceOnALevel1Character_LevelShouldReturn2AsInt()
        {
            // Arrange
            var character = new Mage("Gandalf"); // Mage class is used since we cannot instantiate the from abstract base class Character
            int expected = 2;

            // Act
            character.LevelUp();
            int actual = character.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 3) Each character class(Mage) is created with the proper default attributes.
        ///     - Use level 1 stats for each character as expected.
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Mage_Created_ShouldHaveAttributesStr1Dex1Int8()
        {
            // Arrange
            var character = new Mage("Gandalf");
            var expected = new PrimaryAttributes { Strength = 1, Dexterity = 1, Intelligence = 8 };

            // Act
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }

        /// <summary>
        /// 3) Each character class(Ranger) is created with the proper default attributes.
        ///     - Use level 1 stats for each character as expected.
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Ranger_Created_ShouldHaveAttributesStr1Dex7Int1()
        {
            // Arrange
            var character = new Ranger("Legolas");
            var expected = new PrimaryAttributes { Strength = 1, Dexterity = 7, Intelligence = 1 };

            // Act
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }

        /// <summary>
        /// 3) Each character class(Rogue) is created with the proper default attributes.
        ///     - Use level 1 stats for each character as expected.
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Rogue_Created_ShouldHaveAttributesStr2Dex6Int1()
        {
            // Arrange
            var character = new Rogue("Aragorn");
            var expected = new PrimaryAttributes { Strength = 2, Dexterity = 6, Intelligence = 1 };

            // Act
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }
        /// <summary>
        /// 3) Each character class(Warrior) is created with the proper default attributes.
        ///     - Use level 1 stats for each character as expected.
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Warrior_Created_ShouldHaveAttributesStr5Dex2Int1()
        {
            // Arrange
            var character = new Warrior("Gimli");
            var expected = new PrimaryAttributes { Strength = 5, Dexterity = 2, Intelligence = 1 };

            // Act
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }

        /// <summary>
        /// 4) Each character class (Mage) has their attributes increased when leveling up
        ///     - Create each class once, level them up once.
        ///     - Use the base attributes, plus one instance of the level up as the expected.
        ///     - { Strength = 2, Dexterity = 2, Intelligence = 13 }
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Mage_LevelUpOnce_ShouldHaveAttributesStr2Dex2Int13()
        {
            // Arrange
            var character = new Mage("Gandalf");
            var expected = new PrimaryAttributes { Strength = 2, Dexterity = 2, Intelligence = 13 };

            // Act
            character.LevelUp();
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }

        /// <summary>
        /// 4) Each character class (Ranger) has their attributes increased when leveling up
        ///     - Create each class once, level them up once.
        ///     - Use the base attributes, plus one instance of the level up as the expected.
        ///     - { Strength = 2, Dexterity = 12, Intelligence = 2 }
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Ranger_LevelUpOnce_ShouldHaveAttributesStr2Dex12Int2()
        {
            // Arrange
            var character = new Ranger("Legolas");
            var expected = new PrimaryAttributes { Strength = 2, Dexterity = 12, Intelligence = 2 };

            // Act
            character.LevelUp();
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }

        /// <summary>
        /// 4) Each character class (Rogue) has their attributes increased when leveling up
        ///     - Create each class once, level them up once.
        ///     - Use the base attributes, plus one instance of the level up as the expected.
        ///     - { Strength = 3, Dexterity = 10, Intelligence = 2 }
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Rogue_LevelUpOnce_ShouldHaveAttributesStr3Dex10Int2()
        {
            // Arrange
            var character = new Rogue("Aragorn");
            var expected = new PrimaryAttributes { Strength = 3, Dexterity = 10, Intelligence = 2 };

            // Act
            character.LevelUp();
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }

        /// <summary>
        /// 4) Each character class (Warrior) has their attributes increased when leveling up
        ///     - Create each class once, level them up once.
        ///     - Use the base attributes, plus one instance of the level up as the expected.
        ///     - { Strength = 8, Dexterity = 4, Intelligence = 2 }
        ///     - This results in four test methods.
        /// </summary>
        [Fact]
        public void Warrior_LevelUpOnce_ShouldHaveAttributesStr8Dex4Int2()
        {
            // Arrange
            var character = new Warrior("Gimli");
            var expected = new PrimaryAttributes { Strength = 8, Dexterity = 4, Intelligence = 2 };

            // Act
            character.LevelUp();
            bool IsEqual = character.BaseAttributes.Equals(expected);

            // Assert
            Assert.True(IsEqual);
        }

        /// <summary>
        /// The Display method must return a character sheet.
        /// I decided it would be enough to test if a non empty string is returned, since the whole output would be overkill.
        /// </summary>
        [Fact]
        public void Display_IsCalled_ShouldReturnAStringNotEmpty()
        {
            // Arrange
            var character = new Warrior("Gimli");

            // Act
            string expected = character.Display();

            // Assert
            Assert.NotEmpty(expected);
        }
    }
}