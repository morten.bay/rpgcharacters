﻿using RPGCharacters.Characters.Classes;
using RPGCharacters.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharactersTest
{
    public class ItemTests
    {
        /// <summary>
        /// 1) If a character tries to equip a high level weapon, InvalidWeaponException should be thrown
        ///     - Use the warrior, and the axe, but set the axes level to 2.
        /// </summary>
        [Fact]
        public void CharacterEquip_EquipHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var weapon = predefinedItems.testAxe;
            weapon.Level = 2;

            var expected = "InvalidWeaponException";

            // Act
            var exception = Assert.Throws<InvalidWeaponException>(() => character.Equip(weapon));
            string actual = exception.Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 2) If a character tries to equip a high level armor piece, InvalidArmorException should be thrown
        ///     -  Use the warrior, and the plate body armor, but set the armor’s level to 2.
        /// </summary>
        [Fact]
        public void CharacterEquip_EquipHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var armor = predefinedItems.testPlateBody;
            armor.Level = 2;

            var expected = "InvalidArmorException";

            // Act
            var exception = Assert.Throws<InvalidArmorException>(() => character.Equip(armor));
            string actual = exception.Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 3) If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown
        ///     - Use the warrior and the bow.
        /// </summary>
        [Fact]
        public void CharacterEquip_EquipWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var weapon = predefinedItems.testBow;

            var expected = "InvalidWeaponException";

            // Act
            var exception = Assert.Throws<InvalidWeaponException>(() => character.Equip(weapon));
            string actual = exception.Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 4) If a character tries to equip the wrong armor type, InvalidArmorException should be thrown
        ///     - Use the warrior and the cloth armor.
        /// </summary>
        [Fact]
        public void CharacterEquip_EquipWrongArmorType_ShouldThrowInvalidArmorException()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var armor = predefinedItems.testClothHead;

            var expected = "InvalidArmorException";

            // Act
            var exception = Assert.Throws<InvalidArmorException>(() => character.Equip(armor));
            string actual = exception.Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 5) If a character equips a valid weapon, a success message should be returned
        ///     - "New weapon equipped!"
        /// </summary>
        [Fact]
        public void CharacterEquip_EquipWeapon_ShouldReturnNewWeaponEquippedAsString()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var weapon = predefinedItems.testAxe;

            var expected = "New weapon equipped!";

            // Act
            string actual = character.Equip(weapon);

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 6) If a character equips a valid armor piece, a success message should be returned
        ///     - "New armour equipped!"
        /// </summary>
        [Fact]
        public void CharacterEquip_EquipArmor_ShouldReturnNewArmorEquippedAsString()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var armor = predefinedItems.testPlateBody;

            var expected = "New armor equipped!";

            // Act
            string actual = character.Equip(armor);

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 7) Calculate Damage if no weapon is equipped.
        ///     - Take warrior at level 1
        ///     - Expected Damage = 1*(1 + (5 / 100)) = 1,05
        /// </summary>
        [Fact]
        public void CharacterDPS_DamageIfNoWeaponEquipped_ShouldReturn1point05AsDouble()
        {
            // Arrange
            var character = new Warrior("Gimli");

            double expected = 1.05;

            // Act
            double actual = character.CharacterDPS;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 8) Calculate Damage with valid weapon equipped.
        ///     - Take warrior level 1
        ///     - Equip axe.
        ///     - Expected Damage = (7 * 1.1)*(1 + (5 / 100)) = 8,09
        /// </summary>
        [Fact]
        public void CharacterDPS_DamageWithValidWeaponEquipped_ShouldReturn8point09AsDouble()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var weapon = predefinedItems.testAxe;

            double expected = 8.09; 

            // Act
            character.Equip(weapon);
            double actual = character.CharacterDPS;

            // Assert
            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// 9) Calculate Damage with valid weapon and armor equipped.
        ///     - Take warrior level 1
        ///     - Equip axe.
        ///     - Equip plate body armor.
        ///     - Expected Damage = (7 * 1.1) * (1 + ((5+1) / 100)) = 8,16
        /// </summary>
        [Fact]
        public void CharacterDPS_DamageWithValidWeaponAndArmorEquipped_ShouldReturn8point16AsDouble()
        {
            // Arrange
            var character = new Warrior("Gimli");

            var predefinedItems = new PredefinedItems();
            var weapon = predefinedItems.testAxe;
            var armor = predefinedItems.testPlateBody;

            double expected = 8.16; 

            // Act
            character.Equip(weapon);
            character.Equip(armor);
            double actual = character.CharacterDPS;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
