﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Shared
{
    /// <summary>
    /// This Custom Exception is thown when a character tries to equip a Armor item not possible for a given class
    /// or if the Item level is higher than the Characters level.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {
        }

        public InvalidArmorException(string? message) : base(message)
        {
        }

        public override string Message => "InvalidArmorException";
    }

    /// <summary>
    /// This Custom Exception is thown when a character tries to equip a Weapon item not possible for a given class
    /// or if the Item level is higher than the Characters level.
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {
        }

        public InvalidWeaponException(string? message) : base(message)
        {
        }

        public override string Message => "InvalidWeaponException";
    }
}
