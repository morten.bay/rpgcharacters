﻿using RPGCharacters.Items;
using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using RPGCharacters.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Shared
{
    /// <summary>
    /// A class containing a List of Predifined Game items,
    /// used for the Console "Game" part of the app and
    /// some predefined items used for unit testing.
    /// </summary>
    public class PredefinedItems
    {

        public Item[] GameItemsList = new Item[]
        {
            new Weapon()
            {
                Name = "Common axe",
                Level = 1,
                Slot = Slot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            },
            new Weapon()
            {
                Name = "Common Magic wand",
                Level = 1,
                Slot = Slot.Weapon,
                WeaponType = WeaponType.Wand,
                WeaponAttributes = new WeaponAttributes() { Damage = 2, AttackSpeed = 2 }
            },
            new Weapon()
            {
                Name = "Common dagger",
                Level = 1,
                Slot = Slot.Weapon,
                WeaponType = WeaponType.Dagger,
                WeaponAttributes = new WeaponAttributes() { Damage = 2, AttackSpeed = 10 }
            },
            new Weapon()
            {
                Name = "Common bow",
                Level = 1,
                Slot = Slot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            },
            new Weapon()
            {
                Name = "Hunter bow",
                Level = 6,
                Slot = Slot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 28, AttackSpeed = 0.2 }
            },
            new Weapon()
            {
                Name = "Sharp axe",
                Level = 2,
                Slot = Slot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 1 }
            },
            new Weapon()
            {
                Name = "Sharp Sword",
                Level = 2,
                Slot = Slot.Weapon,
                WeaponType = WeaponType.Sword,
                WeaponAttributes = new WeaponAttributes() { Damage = 22, AttackSpeed = 2 }
            },
            new Armor()
            {
                Name = "Strong leather body armor",
                Level = 3,
                Slot = Slot.Body,
                ArmorType = ArmorType.Leather,
                ArmorAttributes = new PrimaryAttributes() { Strength = 4 }
            },
            new Armor()
            {
                Name = "Strong mail body armor",
                Level = 3,
                Slot = Slot.Body,
                ArmorType = ArmorType.Mail,
                ArmorAttributes = new PrimaryAttributes() { Strength = 5 }
            },
            new Armor()
            {
                Name = "Common plate body armor",
                Level = 1,
                Slot = Slot.Body,
                ArmorType = ArmorType.Plate,
                ArmorAttributes = new PrimaryAttributes() { Strength = 1 }
            },
            new Armor()
            {
                Name = "Common cloth head armor",
                Level = 1,
                Slot = Slot.Head,
                ArmorType = ArmorType.Cloth,
                ArmorAttributes = new PrimaryAttributes() { Intelligence = 5 }
            },
            new Armor()
            {
                Name = "Reliable pants",
                Level = 2,
                Slot = Slot.Legs,
                ArmorType = ArmorType.Cloth,
                ArmorAttributes = new PrimaryAttributes() { Intelligence = 5 }
            }
        };
        public Weapon testAxe = new Weapon()
        {
            Name = "Common axe",
            Level = 1,
            Slot = Slot.Weapon,
            WeaponType = WeaponType.Axe,
            WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
        };

        public Armor testPlateBody = new Armor()
        {
            Name = "Common plate body armor",
            Level = 1,
            Slot = Slot.Body,
            ArmorType = ArmorType.Plate,
            ArmorAttributes = new PrimaryAttributes() { Strength = 1 }
        };

        public Weapon testBow = new Weapon()
        {
            Name = "Common bow",
            Level = 1,
            Slot = Slot.Weapon,
            WeaponType = WeaponType.Bow,
            WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
        };

        public Armor testClothHead = new Armor()
        {
            Name = "Common cloth head armor",
            Level = 1,
            Slot = Slot.Head,
            ArmorType = ArmorType.Cloth,
            ArmorAttributes = new PrimaryAttributes() { Intelligence = 5 }
        };
    }
}
