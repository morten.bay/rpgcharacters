﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Shared
{
    /// <summary>
    /// An enum of the different types of equipment slots.
    /// </summary>
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}
