﻿using RPGCharacters.Characters;
using RPGCharacters.Characters.Classes;
using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using RPGCharacters.Shared;
using RPGCharacters.Game;

namespace RPGCharacters
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // The app is started by initialising a new GameLogic that handles the display and user input.
            var gameLogic = new GameLogic();
            bool GameLoop = true;
            while(GameLoop)
            {
                GameLoop = gameLogic.DisplayActions();
            }
        }
    }
}