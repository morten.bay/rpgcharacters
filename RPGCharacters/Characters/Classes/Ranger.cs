﻿using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters.Classes
{
    /// <summary>
    /// The Ranger Class, inherrited from base Character class.
    /// In the constructor we setup all base attribues and usable items
    /// </summary>
    public class Ranger : Character
    {
        public Ranger(string name) : base(name)
        {
            Class = "Ranger";
            BaseAttributes.Strength = 1;
            BaseAttributes.Dexterity = 7;
            BaseAttributes.Intelligence = 1;

            UseableWeaponTypes = new WeaponType[]
            {
                WeaponType.Bow
                
            };

            UseableArmorTypes = new ArmorType[]
            {
                ArmorType.Leather,
                ArmorType.Mail
            };
        }

        public override double ClassDamageAttribute
        {
            get { return TotalAttributes.Dexterity; }
        }

        public override void LevelUp()
        {
            Level += 1;
            BaseAttributes.Strength += 1;
            BaseAttributes.Dexterity += 5;
            BaseAttributes.Intelligence += 1;
        }
    }
}
