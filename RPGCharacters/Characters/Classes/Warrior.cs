﻿using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters.Classes
{
    /// <summary>
    /// The Warrior Class, inherrited from base Character class.
    /// In the constructor we setup all base attribues and usable items
    /// </summary>
    public class Warrior : Character
    {
        public Warrior(string name) : base(name)
        {
            Class = "Warrior";
            BaseAttributes.Strength = 5;
            BaseAttributes.Dexterity = 2;
            BaseAttributes.Intelligence = 1;

            UseableWeaponTypes = new WeaponType[]
            {
                WeaponType.Axe,
                WeaponType.Hammer,
                WeaponType.Sword
            };

            UseableArmorTypes = new ArmorType[]
            {
                ArmorType.Mail,
                ArmorType.Plate
            };
        }

        public override double ClassDamageAttribute
        {
            get { return TotalAttributes.Strength; }
        }

        public override void LevelUp()
        {
            Level += 1;
            BaseAttributes.Strength += 3;
            BaseAttributes.Dexterity += 2;
            BaseAttributes.Intelligence += 1;
        }
    }
}
