﻿using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters.Classes
{
    /// <summary>
    /// The Rogue Class, inherrited from base Character class.
    /// In the constructor we setup all base attribues and usable items
    /// </summary>
    public class Rogue : Character
    {
        public Rogue(string name) : base(name)
        {
            Class = "Rogue";
            BaseAttributes.Strength = 2;
            BaseAttributes.Dexterity = 6;
            BaseAttributes.Intelligence = 1;

            UseableWeaponTypes = new WeaponType[]
            {
                WeaponType.Dagger,
                WeaponType.Sword

            };

            UseableArmorTypes = new ArmorType[]
            {
                ArmorType.Leather,
                ArmorType.Mail
            };

        }
        public override double ClassDamageAttribute
        {
            get { return TotalAttributes.Dexterity; }
        }

        public override void LevelUp()
        {
            Level += 1;
            BaseAttributes.Strength += 1;
            BaseAttributes.Dexterity += 4;
            BaseAttributes.Intelligence += 1;
        }
    }
}
