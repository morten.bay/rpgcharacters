﻿using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Characters.Classes
{
    /// <summary>
    /// The Mage Class, inherrited from base Character class.
    /// In the constructor we setup all base attribues and usable items
    /// </summary>
    public class Mage : Character
    {
        public Mage(string name) : base(name)
        {
            
            Class = "Mage";
            BaseAttributes.Strength = 1;
            BaseAttributes.Dexterity = 1;
            BaseAttributes.Intelligence = 8;

            UseableWeaponTypes = new WeaponType[]
            {
                WeaponType.Staff,
                WeaponType.Wand
            };

            UseableArmorTypes = new ArmorType[] 
            { 
                ArmorType.Cloth
            };
        }

        public override double ClassDamageAttribute
        {
            get { return TotalAttributes.Intelligence; }
        }
            

        public override void LevelUp()
        {
            Level += 1;
            BaseAttributes.Strength += 1;
            BaseAttributes.Dexterity += 1;
            BaseAttributes.Intelligence += 5;
        }
    }
}
