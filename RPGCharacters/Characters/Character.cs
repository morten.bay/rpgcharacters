﻿using RPGCharacters.Items;
using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using RPGCharacters.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPGCharacters.Characters
{
    /// <summary>
    /// The Character class is the base class for all the other Character classes
    /// </summary>
    public abstract class Character
    {
        public string Name { get; } // Refering to the Name of the character
        public string Class { get; protected set; } // An identifying friendly name of the Class
        public int Level { get; protected set; } // The Characters current level
        public PrimaryAttributes BaseAttributes { get; protected set; } // Characters primary Attributes
        public Dictionary<Slot, Item> Equipment { get; private set; } // The Characters Equippped items
        public WeaponType[]? UseableWeaponTypes { get; protected set; } // The Weapon types valid to a class
        public ArmorType[]? UseableArmorTypes { get; protected set; } // The Armor types valid to a class
        public abstract double ClassDamageAttribute { get; } // The specific Attribute modify that reflects on a Characters total damage
        
        /// <summary>
        /// Getter method calculates the Total Primary Attribues from the Characters BaseAttribues and their equipment
        /// Returns a PrimaryAttribute object with the sum of the individual attributes.
        /// </summary>
        public PrimaryAttributes TotalAttributes
        {
            get
            {
                var total = new PrimaryAttributes();
                total += BaseAttributes;

                foreach (var item in Equipment.Values.OfType<Armor>())
                {
                    total += item.ArmorAttributes!;
                }
                return total;
            }
        }

        /// <summary>
        /// Getter method calculates the DPS of a Character by the formula:
        /// Character damage = Weapon DPS * (1 + TotalPrimaryAttribute/100
        /// returns the calculated DPS as a double
        /// </summary>
        public double CharacterDPS
        {
            get
            {
                double TotalWeaponDPS = 0;
                foreach (var item in Equipment.Values.OfType<Weapon>())
                {
                    TotalWeaponDPS += item.WeaponAttributes!.DPS;
                }

                if (TotalWeaponDPS == 0) // If no weapons are equipped, then unarmed damage is 1
                {
                    TotalWeaponDPS = 1;
                }

                double CharacterDPS = TotalWeaponDPS * (1 + (ClassDamageAttribute / 100));

                return Math.Round(CharacterDPS, 2);
            }
        }

        /// <summary>
        /// Constructor in the base Character class, is required a name as paramenter.
        /// Other properties are initialized.
        /// </summary>
        /// <param name="name"></param>
        public Character(string name)
        {
            Name = name;
            Class = "";
            Level = 1;
            BaseAttributes = new PrimaryAttributes();
            Equipment = new Dictionary<Slot, Item>();
        }

        /// <summary>
        /// This method is responsible for handling the character level up.
        /// Such as increasing the level property and the PrimaryAttributes.
        /// Each Class has different requirements, so it is an abstract method.
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// Allows a Character to equip an Item. 
        /// The input Item's type is determined, and a check is performed to checked if a character can use the item type
        /// and level if they have the required level requirement. 
        /// </summary>
        /// <param name="item"></param>
        /// <returns>A success message</returns>
        /// <exception cref="InvalidArmorException">When a character tries to equip an armor of wrong type or level</exception>
        /// <exception cref="InvalidWeaponException">When a character tries to equip an weapon of wrong type or level</exception>
        /// <exception cref="Exception">If an Item type is not implemented</exception>
        public string Equip(Item item)
        {
            if (item.GetType() == typeof(Armor))
            {
                Armor armor = (Armor)item;

                if (UseableArmorTypes!.Contains(armor.ArmorType) && Level >= armor.Level)
                {
                    Equipment.Remove(armor.Slot);
                    Equipment.Add(armor.Slot, armor);
                    return "New armor equipped!"; //
                }
                else
                {
                    throw new InvalidArmorException();
                }
            }
            else if (item.GetType() == typeof(Weapon))
            {
                Weapon weapon = (Weapon)item;

                if (UseableWeaponTypes!.Contains(weapon.WeaponType) && Level >= weapon.Level)
                {
                    Equipment.Remove(weapon.Slot);
                    Equipment.Add(weapon.Slot, weapon);
                    return "New weapon equipped!";
                }
                else
                {
                    // If the Item Type cannot be determined, a gerneral exception is thrown.
                    throw new InvalidWeaponException();
                }
            }
            else
            {
                throw new Exception("Error, Unknown item type");
            }
        }

        /// <summary>
        /// Displays A characters information, stats and equipment.
        /// </summary>
        /// <returns>A string containing the Character Sheet</returns>
        public string Display()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Name:\t\t {Name}");
            sb.AppendLine($"Class:\t\t {Class}");
            sb.AppendLine($"Level:\t\t {Level}");
            sb.AppendLine($"");
            sb.AppendLine($"Strength:\t {TotalAttributes.Strength}");
            sb.AppendLine($"Dexterity:\t {TotalAttributes.Dexterity}");
            sb.AppendLine($"Intelligence:\t {TotalAttributes.Intelligence}");
            sb.AppendLine($"");
            sb.AppendLine($"Damage:\t\t {CharacterDPS}");
            sb.AppendLine($"");
            sb.AppendLine($"Items:");
            foreach (var item in Equipment.Values)
            {
                sb.AppendLine($"[{item.Slot}] - {item.Name} ");
            }

            return sb.ToString();
        }
    }
}
