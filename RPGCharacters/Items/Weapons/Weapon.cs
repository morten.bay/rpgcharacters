﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items.Weapons
{
    /// <summary>
    /// A Weapon Item.
    /// </summary>
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes? WeaponAttributes { get; set; }


    }
}
