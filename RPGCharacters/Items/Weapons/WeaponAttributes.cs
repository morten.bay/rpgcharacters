﻿namespace RPGCharacters.Items.Weapons
{
    /// <summary>
    /// Weapon Attributes.
    /// </summary>
    public class WeaponAttributes
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS
        {
            get { return Math.Round((Damage * AttackSpeed), 2); }
        }

    }
}