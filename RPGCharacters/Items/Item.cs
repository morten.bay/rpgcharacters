﻿using RPGCharacters.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    /// <summary>
    /// General Item type
    /// </summary>
    public abstract class Item
    {
        public string? Name { get; set; }
        public int Level { get; set; }
        public Slot Slot { get; set; }

    }
}
