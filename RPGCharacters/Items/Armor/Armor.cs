﻿using RPGCharacters.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items.Armor
{
    /// <summary>
    /// A Armor Item
    /// </summary>
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes? ArmorAttributes { get; set; }
    }
}
