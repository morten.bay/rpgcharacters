﻿using RPGCharacters.Characters;
using RPGCharacters.Characters.Classes;
using RPGCharacters.Items;
using RPGCharacters.Items.Armor;
using RPGCharacters.Items.Weapons;
using RPGCharacters.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Game
{
    /// <summary>
    /// The GameLogic class is responsible for the console applicaiton user interface.
    /// It handles the display/game logic and user input.
    /// </summary>
    public class GameLogic
    {
        public Character _Character { get; set; }

        /// <summary>
        /// When a new instance of GameLogic is created, a new character is also created and
        /// saved to a class propperty for easy method access.
        /// </summary>
        public GameLogic()
        {
            string name = EnterName();
            _Character = SelectHero(name);
        }

        /// <summary>
        /// Displays a user prompt to enter a name and returns that as a string
        /// </summary>
        /// <returns>The name entered by the user</returns>
        public static string EnterName()
        {
            bool stayInMenu = true;
            string name = "";

            while (stayInMenu)
            {
                Console.WriteLine("Hello, please enter a of your Hero!");
                name = Console.ReadLine()!;
                if (string.IsNullOrEmpty(name))
                {
                    Console.WriteLine("Invalid input");
                }
                else
                {
                    stayInMenu = false;
                }
            }
            return name!;
        }

        /// <summary>
        /// Displays a user prompt to select the type of character to create
        /// </summary>
        /// <param name="name">The name of the character to create</param>
        /// <returns>A Character object of the chosen class</returns>
        public static Character SelectHero(string name)
        {
            Console.Clear();
            Console.WriteLine($"Hello {name}, please choose your class.\n");
            Console.WriteLine($"1. Mage");
            Console.WriteLine($"2. Ranger");
            Console.WriteLine($"3. Rogue");
            Console.WriteLine($"4. Warrior");

            Character character = null!;
            bool stayInMenu = true;

            while (stayInMenu)
            {
                string selection = Console.ReadLine()!;

                switch (selection)
                {
                    case "1":
                        character = new Mage(name);
                        stayInMenu = false;
                        break;
                    case "2":
                        character = new Ranger(name);
                        stayInMenu = false;
                        break;
                    case "3":
                        character = new Rogue(name);
                        stayInMenu = false;
                        break;
                    case "4":
                        character = new Warrior(name);
                        stayInMenu = false;
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        break;
                }
            }
            return character;
        }

        /// <summary>
        /// Displays a main menu where the user can select various actions
        /// </summary>
        /// <returns>True or False depending if the user wants to quit the game</returns>
        public bool DisplayActions()
        {
            Console.Clear();
            Console.WriteLine($"Select actions to perform\n");
            Console.WriteLine($"1. Display Character Stats");
            Console.WriteLine($"2. Level up");
            Console.WriteLine($"3. Equip Items");
            Console.WriteLine($"4. Quit game");

            string selection = Console.ReadLine()!;

            switch (selection)
            {
                case "1":
                    DisplayHero();
                break;
                case "2":
                    _Character.LevelUp();
                    Console.WriteLine("\nCharacter gained a level!\n");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    break;
                case "3":
                    DisplayAvailableItems();
                    break;
                case "4":
                    return false;
                default:
                    Console.WriteLine("Invalid input");
                    break;
            }

            return true;
        }

        /// <summary>
        /// Displays the Characters Stats.
        /// </summary>
        public void DisplayHero()
        {
            Console.Clear();
            Console.WriteLine("Character Stats:\n");
            Console.WriteLine(_Character.Display());
            Console.WriteLine("Press any key to return to menu");
            Console.ReadKey();
        }

        /// <summary>
        /// Displays a selected Item and allows the user to try and equip the item.
        /// </summary>
        /// <param name="item"></param>
        /// <exception cref="Exception">Thrown if an unknown Item is passed</exception>
        public void DisplayItem(Item item)
        {
            Console.Clear();
            Console.WriteLine("Item Stats:\n");

            bool stayInMenu = true;

            while (stayInMenu)
            {
                Console.Clear();
                if (item.GetType() == typeof(Armor))
                {
                    Armor armor = (Armor)item;
                    Console.WriteLine($"Name:\t\t {armor.Name}");
                    Console.WriteLine($"Type:\t\t {armor.ArmorType}");
                    Console.WriteLine($"Level:\t\t {armor.Level}");
                    Console.WriteLine($"Slot:\t\t {armor.Slot}");
                    Console.WriteLine($"Strength:\t {armor.ArmorAttributes!.Strength}");
                    Console.WriteLine($"Dexterity:\t {armor.ArmorAttributes!.Dexterity}");
                    Console.WriteLine($"Intelligence:\t {armor.ArmorAttributes!.Intelligence}");

                }
                else if (item.GetType() == typeof(Weapon))
                {
                    Weapon weapon = (Weapon)item;
                    Console.WriteLine($"Name:\t\t {weapon.Name}");
                    Console.WriteLine($"Type:\t\t {weapon.WeaponType}");
                    Console.WriteLine($"Level:\t\t {weapon.Level}");
                    Console.WriteLine($"Slot:\t\t {weapon.Slot}");
                    Console.WriteLine($"Damage:\t\t {weapon.WeaponAttributes!.Damage}");
                    Console.WriteLine($"Attack Speed:\t {weapon.WeaponAttributes!.AttackSpeed}");
                    Console.WriteLine($"DPS:\t\t {weapon.WeaponAttributes!.DPS}");
                }
                else
                {
                    throw new Exception("Error, Unknown item type");
                }

                Console.WriteLine("\nType [e] to equip this item or [b] to go back to the previous menu.");
                string selection = Console.ReadLine()!;

                switch (selection)
                {
                    case "e":
                        try
                        {
                            Console.WriteLine($"\n{_Character.Equip(item)}");
                            Console.WriteLine("\nPress any key to continue");
                            Console.ReadKey();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            Console.WriteLine("Press any key to try again");
                            Console.ReadKey();
                        }
                        stayInMenu = false;
                        break;
                    case "b":
                        stayInMenu = false;
                        break;
                    default:
                        Console.WriteLine("Invalid input");
                        Console.WriteLine("Press any key to try again");
                        Console.ReadKey();
                        break;
                }
            }
        }

        /// <summary>
        /// Displays all available items from the PredefinedItems list.
        /// And allows the user to view and then equip an item.
        /// </summary>
        public void DisplayAvailableItems()
        {
            var itemList = new PredefinedItems().GameItemsList;
            bool stayInMenu = true;

            while (stayInMenu)
            {
                Console.Clear();
                Console.WriteLine("Available Items:\n");
                for (int i = 0; i < itemList.Length; i++)
                {
                    Console.WriteLine($"{i}. {itemList[i].Name}");
                }
                Console.WriteLine($"\nType [0]-[{itemList.Length-1}] to View/Equip an item or [b] to go back to the previous menu.");
                string input = Console.ReadLine()!;
                int selection;

                if (int.TryParse(input, out selection) && selection >= 0 && selection < itemList.Length)
                {
                    DisplayItem(itemList[selection]);
                }
                else if (input == "b")
                {
                    stayInMenu = false;
                }
                else
                {
                    Console.WriteLine("Invalid input");
                    Console.WriteLine("Press any key to return to menu");
                    Console.ReadKey();
                }
            }
        }
    }
}
